#!/bin/bash

set -u

do_cleanup() {
  local cnt
  cnt=$((${#cleanup[*]}-1))
  for i in $(seq ${cnt} -1 0); do
    echo "* ${cleanup[$i]}"
    eval "${cleanup[$i]}" || true
  done
  echo "done."
}

declare -a cleanup
cleanup+=(":")
trap do_cleanup EXIT

if [[ -n "$(git status -s)" ]]; then
  echo >&2 "Git tree is not clean."
  exit 1
fi

# Set temporary changelog entry and snapshot version
ts=$(date -u +'%Y%m%dT%H%M%S')
dv=$(dpkg-parsechangelog | grep-dctrl -n -s Version '')
if ! [[ $dv =~ ^[0-9]+$ ]]; then
  echo >&2 "Package version is already non-simple ($dv)."
  exit 1
fi
cleanup+=("git checkout debian/changelog")
dch --newversion "$dv~$ts.1" --force-bad-version --distribution unstable "Snapshot build by ${USER}"
newdv=$(dpkg-parsechangelog | grep-dctrl -n -s Version '')

# Check if we already have files named like this
glob="$(echo 'debian.org[_-]*[_.]*'{build,buildinfo,changes,dsc,tar.gz,deb})"
if ! [ "$glob" = "$(cd .. && echo $glob)" ]; then
  echo "Expected packages glob '$glob' already matches things!"
  exit 1
fi

# build the packages
./make-deb

# check if we did what we expected
if [ "$glob" = "$(cd .. && echo $glob)" ]; then
  echo "No matches for glob '$glob' -- did we not build anything?"
  exit 1
fi

# optionally install packages
if [ "$#" -gt 0 ]; then
  while [ "$#" -gt 0 ]; do
    pkg="$1"; shift
    (cd .. && set -x &&
     rsync --verbose --progress --times "debian.org-${pkg}_${newdv}_"* draghi.debian.org:/srv/db.debian.org/ftp-archive/archive/pool/debian-all/debian.org-x &&
     ssh draghi.debian.org 'chronic make -C /srv/db.debian.org/ftp-archive')
  done
  (cd .. && echo "Removing built packages" && rm $glob)
else
  echo "Leaving packages behind because you did not ask for one to be installed."
  echo "To clean up:"
  echo "  bash -c 'cd .. && rm -v $glob'"
fi
