#!/bin/bash
# 
#  apt-in-chroot - runs apt in a specified chroot
# 
#  Copyright (C) 2010 Martin Zobel-Helas <zobel@debian.org>
#  Copyright (C) 2012 Peter Palfrader <peter@palfrader.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2, or (at your option)
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA.
# 

PATH="/usr/sbin:/usr/bin:/sbin:/bin"
export PATH

usage() {
        bn="`basename "$0"`"
	echo "Usage: $bn <chroot> <apt sub-command> [<package>]"
	echo ""
	echo "  allowed apt sub-commands are:"
	echo "          + install"
	echo "          + remove"
	echo "          + purge"
	echo "          + build-dep"
	echo "          + update"
	echo "          + upgrade"
	echo "          + dist-upgrade"
	echo "          + clean"
}


if [ "$#" -lt "2" ]; then
	usage >&2
	exit 1
fi

CHROOT_DIR="/chroot"
CHROOT="$1"; shift
APTCMD="$1"; shift
PACKAGES="$*"

declare -a suffix
suffix[0]=""

if [ -x /usr/bin/dchroot ] ; then
	chroots=$(dchroot -l 2>&1 | awk -F": " '{print $2}' | tr ' ' '\n')
	method=dchroot
	suffix[${#suffix[*]}]="_$(dpkg --print-architecture)"
	allowed_filter='.'
elif [ -x /usr/bin/schroot ] ; then
	chroots=$(schroot -l | awk -F":" '{print $2}' | grep -- '-dchroot$' )
	method=schroot
	suffix[${#suffix[*]}]=""
	suffix[${#suffix[*]}]="-dchroot"
	suffix[${#suffix[*]}]="-$(dpkg --print-architecture)-dchroot"
	allowed_filter='-dchroot$'
else
	echo >&2 "Cannot find chroot wrapper."
	exit 1
fi

requested_chroot=""
while read c; do
	for (( i = 0 ; i < ${#suffix[*]} ; i++ )); do
		if [ "$c" == "$CHROOT${suffix[$i]}" ]; then
			requested_chroot="$c"
		fi
	done
done <<< "$chroots"

if ! [ -n "$requested_chroot" ]; then
	echo >&2 "$CHROOT is not a valid dchroot.  Available chroots are:"
	echo "$chroots"
	exit 1
elif ! [[ "$requested_chroot" =~ $allowed_filter ]]; then  # do not quote the regex
	echo >&2 "$CHROOT is not a valid dchroot.  While it exists, this script may not touch it.  Chroot names must match $allowed_filter."
	exit 1
fi


case "$APTCMD" in
install|remove|purge|build-dep|update|upgrade|dist-upgrade|clean)
	# those are the allowed apt sub-commands
	;;
*)
	echo "$APTCMD is not a valid apt sub-command"
	exit 1
	;;
esac

# valid chroot
if [ "$method" = "dchroot" ]; then
	echo "Will run '/usr/sbin/chroot $CHROOT_DIR/$requested_chroot apt-get $APTCMD $PACKAGES'"
	/usr/sbin/chroot $CHROOT_DIR/"$requested_chroot" apt-get "$APTCMD" $PACKAGES
elif [ "$method" = "schroot" ]; then
	echo "Will run 'schroot -c $requested_chroot apt-get $APTCMD $PACKAGES'"
	schroot -c "$requested_chroot" apt-get "$APTCMD" $PACKAGES
else
	echo >&2 "Invalid method."
	exit 1
fi
